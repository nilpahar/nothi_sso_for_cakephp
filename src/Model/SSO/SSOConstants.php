<?php

namespace SSOLogin\App\Model\SSO;

class SSOConstants
{
//    const APP_ID = "3KLTN9eRRQYtxZKogwcAz1E9OBJk63p7";
    const APP_ID = "BXjLsYAa6n6n2kgJ6tNApUBCKhhHe067";
    const APP_NAME = "Nothi";
//    const SHARED_SECRET = "H0Grz55TPuJYdxP6Mg1oMyEiUOwpfyo0JDPWSoFSwyGmWeU8GN9P42gqOvOV9nVrhbKyS3ZuoguCmhKxkuChv9zrEK2hwBpe4pDujAJGckJz6ZlI1wFphpvRKzpkukb35o7IOSVhdC1w1pqXWb3Gk6tQgDr97cUV9HchajVbtE1ELDUmubzsCGvAarqP8E8BxbzJYWMnLICPcQ9UhjESiw1ZbNT1Qnd1v0ifkohl2lZIzPStzOfXGFkrUi0orw3x";
    const SHARED_SECRET = "v0RDVCdjojHCFNnUv8QvyZhCdDpfiteDKy1adxW67lTccFYc4DyYlDqvsI2Sf8sBLbeYxo76bLnSqkRyMmfkfsQ5KIyiJfNmF8NTTcjwsZqX3LMq2Q26eE8Qj8jDWstBNLEnBN25HljNp9XvNWhWO8ChfF5JIll0wfTkFplY0Q05QW10bUoQYk99VtPgceQrhmoFmp6EDeoeVgcWYGL6iialZAumUSwY6bWosPB8fBAinYlqkuRqfa8QhS8QXP7V";
    const IDP_URL = "http://account.beta.doptor.gov.bd";
    const APP_NAME_QS = "appName";
    const APP_ID_QS = "appId";
    const APP_LOGIN_ENDPOINT ="applogin";
    const IA_LOGIN_ENDPOINT = "interapplogin";
	const AUTHORIZE_END_POINT = "authorize";
	const SLO_END_POINT = "ssologout";
    const TOKEN_EXP_DATE = "180000";
//	const LOGIN_PAGE_URI = "http://103.48.18.21:8080/nothi_new_sso/";
    const REDIRECT_URI = "http://training.nothi.gov.bd/";
    const LANDING_PAGE_URI = "http://training.nothi.gov.bd/dashboard";
    const LOGIN_PAGE_URI = "http://training.nothi.gov.bd/";

    const USERNAME = "username";
    const EMPLOYEE_RECORD_ID = "employee_record_id";
    const OFFICE_ID = "office_id";
    const DESIGNATION = "designation";
    const OFFICE_UNIT_ID = "office_unit_id";
    const INCHARGE_LABEL = "incharge_label";
    const OFFICE_UNIT_ORGANOGRAM_ID = "office_unit_organogram_id";
    const OFFICE_NAME_ENG = "office_name_eng";
    const OFFICE_NAME_BNG = "office_name_bng";
    const OFFICE_MINISTRY_ID = "office_ministry_id";
    const OFFICE_MINISTRY_NAME_ENG = "office_ministry_name_eng";
    const OFFICE_MINISTRY_NAME_BNG = "office_ministry_name_bng";
    const UNIT_NAME_ENG = "unit_name_eng";
    const UNIT_NAME_BNG = "unit_name_bng";
    const FROM_APP_NAME = "fromAppName";
    const FROM_APP_ID = "fromAppId";
    const TO_APP_NAME = "toAppName";
    const TO_APP_ID = "toAppId";
    const LANDING_PAGE_URL = "landingpageurl";
    const TOKEN_EXP_TIME_TEXT = "expirationTime";
}
