<?php

namespace SSOLogin\App\Model\SSO\Lib\Interfaces;

use Cake\Controller\Controller;

interface ISSOAppLogin
{
    public function getLandingUrl(Controller $object);
}
