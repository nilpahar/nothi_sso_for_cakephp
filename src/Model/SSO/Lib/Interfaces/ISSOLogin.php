<?php


namespace SSOLogin\App\Model\SSO\Lib\Interfaces;

use Cake\Controller\Controller;

interface ISSOLogin
{
    public function getRedirectUrl(Controller $request);
}
