<?php

namespace SSOLogin\App\Model\SSO\Lib;

use SSOLogin\App\Model\SSO\AppLoginResponse;
use SSOLogin\App\Model\SSO\Lib\Interfaces\ISSOAppLogin;
use Cake\Controller\Controller;
use Exception;

class SSOAppLogin implements ISSOAppLogin
{
    public function __construct()
    {
    }

    public function getLandingUrl(Controller $object)
    {
        try {
            $token = $object->request->getData(LibConstants::TOKEN);
            $nonce = $object->request->getSession()->read(LibConstants::NONCE);

            $appLoginResponse = new AppLoginResponse();
            $response = $appLoginResponse->parseResponse($token, $nonce);

            $object->request->getSession()->write(LibConstants::USER_SESSION_KEY, $token);
            $object->request->getSession()->write(LibConstants::SSO_DESIGNATION, $response->getOfficeUnitOrganogramId());

            return $response->getLandingPageUrl();
        } catch (Exception $e) {
            return null;
        }
    }
}
