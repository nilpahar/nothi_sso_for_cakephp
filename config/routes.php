<?php

use Cake\Routing\Route\DashedRoute;
use Cake\Routing\Router;

Router::plugin(
    'SSOLogin',
    ['path' => '/sso'],
    function ($routes) {
        $routes->connect('/login', ['controller' => 'SSO', 'action' => 'login'],['_name'=>'sso-login']);
        $routes->connect('/applogin', ['controller' => 'SSO', 'action' => 'applogin']);
    }
);
Router::plugin(
    'SSOLogin',
    ['path' => '/'],
    function ($routes) {
        $routes->connect('/applogin', ['controller' => 'SSO', 'action' => 'applogin']);
    }
);
