# Single Sign On (SSO) on Nothi For CakePHP
 
## Requirement
1. PHP > 7.2
2. CakePHP > 4.0


## Installation 
0. After download/clone, rename directory to `SSOLogin`

0. Copy directory to `plugins` directory
 
0. Add configuration on SSO in `app_local.php` for development and `app.php` for production mode 
    ```php
    'sso' => true,
    ```

0. Add in `config/routes.php`

    ```php
    if (Cake\Core\Configure::read('sso')) { 
        $builder->loadPlugin('SSOLogin');   
    }
    ```

0. Add this line in `src/Application.php` in `bootstrap()` method

    ```php
    if (Cake\Core\Configure::read('sso')) {
        $this->addPlugin('SSOLogin', ['routes' => true]);
    }
    ```

0. Add or update `src/Controller/AppController.php` in `initialize()` method
    ```php
   if (Cake\Core\Configure::read('sso')) { 
       $this->loadComponent('Auth', [
            'loginRedirect' => ['_name'=>'dashboard'],
            // you can define your dashboard url
            'logoutRedirect' => [
                'controller' => 'sso',
                'action' => 'login'
            ],
            'loginAction' => [
                'controller' => 'sso',
                'action' => 'login'
            ]
        ]);
   }
    ```

0. Add this line in `composer.json` in `autoload` -> `psr-4`

    ```bash
    "SSOLogin\\": "plugins/SSOLogin/src/"
    ```

0. After that run this command

    ```bash
    composer dump-autoload
    ```
 
 ###### Maintained by [Tappware Solutions Ltd](https://www.tappware.com)
