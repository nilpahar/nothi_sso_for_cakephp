<!DOCTYPE html>
<html lang="en">
<head>
    <title>
        <?php
        if (!empty($this->request->getData('title'))) {
            echo $this->request->getData('title');
        } else {
            echo Cake\Core\Configure::read('App.name');
        }
        ?>
    </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="/SSOLogin/sso_login/images/icons/favicon.ico"/>
    <link rel="stylesheet" type="text/css" href="/SSOLogin/sso_login/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/SSOLogin/sso_login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/SSOLogin/sso_login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <link rel="stylesheet" type="text/css" href="/SSOLogin/sso_login/vendor/animate/animate.css">
    <link rel="stylesheet" type="text/css" href="/SSOLogin/sso_login/vendor/css-hamburgers/hamburgers.min.css">
    <link rel="stylesheet" type="text/css" href="/SSOLogin/sso_login/vendor/animsition/css/animsition.min.css">
    <link rel="stylesheet" type="text/css" href="/SSOLogin/sso_login/vendor/select2/select2.min.css">
    <link rel="stylesheet" type="text/css" href="/SSOLogin/sso_login/vendor/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="/SSOLogin/sso_login/css/util.css">
    <link rel="stylesheet" type="text/css" href="/SSOLogin/sso_login/css/main.css">
</head>
<body>

<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-50">
            <?= $this->Flash->render(); ?>
            <?= $this->fetch('content'); ?>
        </div>
    </div>
</div>



<script src="/SSOLogin/sso_login/vendor/jquery/jquery-3.2.1.min.js"></script>
<script src="/SSOLogin/sso_login/vendor/animsition/js/animsition.min.js"></script>
<script src="/SSOLogin/sso_login/vendor/bootstrap/js/popper.js"></script>
<script src="/SSOLogin/sso_login/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="/SSOLogin/sso_login/vendor/select2/select2.min.js"></script>
<script src="/SSOLogin/sso_login/vendor/daterangepicker/moment.min.js"></script>
<script src="/SSOLogin/sso_login/vendor/daterangepicker/daterangepicker.js"></script>
<script src="/SSOLogin/sso_login/vendor/countdowntime/countdowntime.js"></script>
<script src="/SSOLogin/sso_login/js/main.js"></script>

</body>
</html>
