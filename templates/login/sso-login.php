<?=$this->Form->create(null, ['class' => 'login-form', 'url' => $idpLoginURL, 'autocomplete' => 'off']);?>
<?php
echo $this->Form->hidden('fingerprint', ['id' => 'fingerprint']);
echo $this->Form->hidden('client_id', ['value' => isset($client_id)?$client_id:'']);
echo $this->Form->hidden('landing_page_uri', ['value' => isset($landing_page_uri)?$landing_page_uri:'']);
echo $this->Form->hidden('nonce', ['value' => isset($nonce)?$nonce:'']);
?>
    <span class="login100-form-title p-b-33">
        Account Login
    </span>

    <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
        <input class="input100" type="text" name="name" placeholder="Username" autocomplete = "off">
        <span class="focus-input100-1"></span>
        <span class="focus-input100-2"></span>
    </div>

    <div class="wrap-input100 rs1 validate-input" data-validate="Password is required">
        <input class="input100" type="password" name="password" placeholder="Password">
        <span class="focus-input100-1"></span>
        <span class="focus-input100-2"></span>
    </div>

    <div class="container-login100-form-btn m-t-20">
        <button class="login100-form-btn">
            Sign in
        </button>
    </div>

    <div class="text-center p-t-45 p-b-4">
        <span class="txt1">
            Forgot
        </span>

        <a href="#" class="txt2 hov1">
            Username / Password?
        </a>
    </div>

    <div class="text-center">
        <span class="txt1">
            Create an account?
        </span>

        <a href="#" class="txt2 hov1">
            Sign up
        </a>
    </div>
</form>
